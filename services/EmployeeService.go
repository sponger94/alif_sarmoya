package services

import (
	"fmt"
	"gitlab.com/sponger94/alif_sarmoya/interfaces"
)

type EmployeeService struct {
	interfaces.IEmployeeRepository
}

func (service *EmployeeService) GetMaxSalaryEmployee() (string, error) {
	employee, err := service.GetEmployeeByName("Javoha")
	if err != nil {
		fmt.Println(err)
		return "", nil
	}

	return employee.FirstName, nil
}
