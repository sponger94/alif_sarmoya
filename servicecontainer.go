package main

import (
	"sync"

	"gitlab.com/sponger94/alif_sarmoya/controllers"
	"gitlab.com/sponger94/alif_sarmoya/databases"
	"gitlab.com/sponger94/alif_sarmoya/repositories"
	"gitlab.com/sponger94/alif_sarmoya/services"
)

type IServiceContainer interface {
	InjectEmployeeController() controllers.EmployeeController
}

type kernel struct{}

func (k *kernel) InjectEmployeeController() controllers.EmployeeController {
	//sqlConn, _ := sql.Open("postgres", "host=127.0.0.1 port=5432 user=root dbname=alif password=123")
	postgreSqlHandler := databases.NewPostgreSqlHandler("postgres", "host=127.0.0.1 port=5432 user=root dbname=alif password=123 sslmode=disable")

	employeeRepository := &repositories.EmployeeRepository{postgreSqlHandler}
	employeeService := &services.EmployeeService{employeeRepository}
	employeeController := controllers.EmployeeController{employeeService}

	return employeeController
}

var (
	k             *kernel
	containerOnce sync.Once
)

func ServiceContainer() IServiceContainer {
	if k == nil {
		containerOnce.Do(func() {
			k = &kernel{}
		})
	}
	return k
}
