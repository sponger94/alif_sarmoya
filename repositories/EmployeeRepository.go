package repositories

import (
	"fmt"
	"gitlab.com/sponger94/alif_sarmoya/interfaces"
	"gitlab.com/sponger94/alif_sarmoya/models"
)

type EmployeeRepository struct {
	interfaces.IDbHandler
}

func (repository *EmployeeRepository) GetEmployeeByName(name string) (models.EmployeeModel, error) {
	row, err := repository.Query(fmt.Sprintf("SELECT * FROM employees WHERE first_name = '%s", name))
	if err != nil {
		return models.EmployeeModel{}, err
	}

	var employee models.EmployeeModel
	row.Next()
	row.Scan(&employee.Id, &employee.FirstName, &employee.Salary)
	return employee, nil
}

func (repository *EmployeeRepository) GetEmployeeWithMaxSalary() (models.EmployeeModel, error) {
	//TODO: Make this happen
	row, err := repository.Query("SELECT * FROM employees WHERE first_name = 'Javoha'")
	if err != nil {
		return models.EmployeeModel{}, err
	}

	var employee models.EmployeeModel
	row.Next()
	row.Scan(&employee.Id, &employee.FirstName, &employee.Salary)
	return employee, nil
}
