package main

import (
	"sync"

	"github.com/go-chi/chi"
)

type IChiRouter interface {
	InitRouter() *chi.Mux
}

type router struct{}

func (router *router) InitRouter() *chi.Mux {
	employeeController := ServiceContainer().InjectEmployeeController()

	r := chi.NewRouter()
	r.HandleFunc("/getEmployeeWithHighestSalary", employeeController.GetEmployeeWithMaxSalary)

	return r
}

var (
	m          *router
	routerOnce sync.Once
)

func ChiRouter() IChiRouter {
	if m == nil {
		routerOnce.Do(func() {
			m = &router{}
		})
	}
	return m
}
