package controllers

import (
	"encoding/json"
	"fmt"
	"gitlab.com/sponger94/alif_sarmoya/interfaces"
	"gitlab.com/sponger94/alif_sarmoya/viewmodels"
	"net/http"
)

type EmployeeController struct {
	interfaces.IEmployeeService
}

func (controller *EmployeeController) GetEmployeeWithMaxSalary(res http.ResponseWriter, req *http.Request) {
	employee, err := controller.GetMaxSalaryEmployee()
	if err != nil {
		//Handle the error
		fmt.Println(err)
	}

	json.NewEncoder(res).Encode(viewmodels.TopSalaryVM{employee})
}
