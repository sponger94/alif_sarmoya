package interfaces

import (
	"gitlab.com/sponger94/alif_sarmoya/models"
)

type IEmployeeRepository interface {
	GetEmployeeByName(name string) (models.EmployeeModel, error)
	GetEmployeeWithMaxSalary() (models.EmployeeModel, error)
}
