package interfaces

type IEmployeeService interface {
	GetMaxSalaryEmployee() (string, error)
}
