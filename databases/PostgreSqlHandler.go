package databases

import (
	"database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/sponger94/alif_sarmoya/interfaces"
	"gitlab.com/sponger94/alif_sarmoya/models"
)

type PostgreSqlHandler struct {
	Conn *sql.DB
}

func (handler *PostgreSqlHandler) Execute(statement string) {
	handler.Conn.Exec(statement)
}
func (handler *PostgreSqlHandler) Query(statement string) (interfaces.IRow, error) {
	rows, err := handler.Conn.Query(statement)
	if err != nil {
		fmt.Println(err)
		return new(SqliteRow), err
	}
	row := new(SqliteRow)
	row.Rows = rows
	return row, nil
}

type SqliteRow struct {
	Rows *sql.Rows
}

func (r SqliteRow) Scan(dest ...interface{}) error {
	err := r.Rows.Scan(dest...)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

func (r SqliteRow) Next() bool {
	return r.Rows.Next()
}

func NewPostgreSqlHandler(dialect string, cn string) *PostgreSqlHandler {
	db, err := gorm.Open(dialect, cn)
	if err != nil {
		//handle
		fmt.Println(err)
	}
	postgreSqlHandler := &PostgreSqlHandler{}
	postgreSqlHandler.Conn = db.DB()
	db.AutoMigrate(&models.EmployeeModel{})
	return postgreSqlHandler
}
